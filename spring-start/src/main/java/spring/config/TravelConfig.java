package spring.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import java.util.ArrayList;
import java.util.List;

@Configuration
@ComponentScan("spring")
//@PropertySource("classpath:/application.properties")
public class TravelConfig {

    @Bean
    String travelName(){
        return "Summer holiday 2024";
    }

    @Bean("meals")
    List<String> meals(){
        List<String> meals = new ArrayList<>();
        meals.add("gazpacho");
        meals.add("spaghetti");
        meals.add("tiramisu");
        return meals;
    }
}
