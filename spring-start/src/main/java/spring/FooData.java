package spring;

import lombok.*;

import java.util.Objects;

//@ToString
//@EqualsAndHashCode
//@Getter
//@Setter
//@NoArgsConstructor
@AllArgsConstructor
@Data
@RequiredArgsConstructor
public class FooData {

    private int foo1;
    private String foo2;
    private final long timestamp;

/*    public FooData(int foo1, String foo2) {
        this.foo1 = foo1;
        this.foo2 = foo2;
    }*/

/*    public FooData() {
    }*/

/*    public int getFoo1() {
        return foo1;
    }

    public void setFoo1(int foo1) {
        this.foo1 = foo1;
    }*/

/*    public String getFoo2() {
        return foo2;
    }

    public void setFoo2(String foo2) {
        this.foo2 = foo2;
    }*/
/*
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FooData fooData = (FooData) o;

        if (foo1 != fooData.foo1) return false;
        return Objects.equals(foo2, fooData.foo2);
    }

    @Override
    public int hashCode() {
        int result = foo1;
        result = 31 * result + (foo2 != null ? foo2.hashCode() : 0);
        return result;
    }*/

/*    @Override
    public String toString() {
        return "FooData{" +
                "foo1=" + foo1 +
                ", foo2='" + foo2 + '\'' +
                '}';
    }*/
}
