package spring;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import org.springframework.stereotype.Component;

@Component("travel")
@RequiredArgsConstructor
@ToString
public class Travel {

    @Getter
    private final String name /*= "N1"*/;
    private final Transportation transportation;
    private final Accomodation accomodation;

    //@Autowired
    /*public Travel(Transportation transportation, Accomodation accomodation, String name) {
        this.transportation = transportation;
        this.accomodation = accomodation;
        this.name = name;
        System.out.println("constructing trip with parametrized constructor...");
    }*/

    public void travel(Person p){
        System.out.println("started travel " + name + " for a person " + p);
        transportation.transport(p);
        accomodation.host(p);
        transportation.transport(p);
    }

/*
    public String getName() {
        return name;
    }
*/

/*    @Override
    public String toString() {
        return "Travel{" +
                "name='" + name + '\'' +
                ", transportation=" + transportation +
                ", accomodation=" + accomodation +
                '}';
    }*/
}
