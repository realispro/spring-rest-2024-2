package spring;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Component
@Slf4j
@RequiredArgsConstructor
public class TravelRunner implements CommandLineRunner {

    private final Travel travel;

    @Override
    public void run(String... args) throws Exception {

        log.info("Kowalski will travel");
        Person kowalski = new Person("Jan", "Kowalski", new Ticket(LocalDate.now().minusDays(1)));
        travel.travel(kowalski);
    }
}
