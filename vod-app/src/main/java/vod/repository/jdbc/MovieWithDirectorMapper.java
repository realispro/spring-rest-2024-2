package vod.repository.jdbc;

import org.springframework.jdbc.core.RowMapper;
import vod.model.Director;
import vod.model.Movie;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

public class MovieWithDirectorMapper implements RowMapper<Movie> {
    @Override
    public Movie mapRow(ResultSet rs, int rowNum) throws SQLException {

        Movie m = new Movie();
        m.setId(rs.getInt("movie_id"));
        m.setTitle(rs.getString("movie_title"));
        m.setPoster(rs.getString("movie_poster"));
        Director director = new Director();
        director.setId(rs.getInt("movie_director_id"));
        director.setFirstName(rs.getString("director_firstname"));
        director.setLastName(rs.getString("director_lastname"));
        m.setDirector(director);
        return m;
    }
}
