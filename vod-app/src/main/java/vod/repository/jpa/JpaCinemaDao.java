package vod.repository.jpa;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;
import vod.model.Cinema;
import vod.model.Movie;
import vod.repository.CinemaDao;

import java.util.List;
import java.util.Optional;

@Repository
@Primary
public class JpaCinemaDao implements CinemaDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Cinema> findAll() {
        return entityManager.createQuery("select c from Cinema c").getResultList();
    }

    @Override
    public Optional<Cinema> findById(Integer id) {
        return Optional.ofNullable(
            entityManager.find(Cinema.class, id)
        );
    }

    @Override
    public List<Cinema> findByMovie(Movie m) {
        return entityManager.createQuery("select c from Cinema c join c.movies m where m=:movie")
                .setParameter("movie", m)
                .getResultList();
    }

    @Override
    public Cinema save(Cinema c) {
        entityManager.persist(c);
        return c;
    }
}
