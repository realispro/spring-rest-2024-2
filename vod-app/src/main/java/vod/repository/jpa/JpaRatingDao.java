package vod.repository.jpa;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;
import vod.model.Movie;
import vod.model.Rating;
import vod.repository.RatingDao;

import java.util.List;
@Repository
@Primary
public class JpaRatingDao implements RatingDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Rating save(Rating rating) {
        entityManager.persist(rating);
        return rating;
    }

    @Override
    public List<Rating> findAllByMovie(Movie movie) {
        return entityManager.createQuery("select r from Rating r where r.movie=:movie")
                .setParameter("movie", movie)
                .getResultList();
    }
}
