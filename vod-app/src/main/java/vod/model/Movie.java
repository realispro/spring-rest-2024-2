package vod.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.validator.constraints.URL;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@ToString(exclude = "cinemas")
@Entity
public class Movie {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @NotNull
    private String title;
    @URL
    private String poster;
    @ManyToOne
    //@JoinColumn(name = "director_id")
    private Director director;
    @JsonIgnore
    @ManyToMany(mappedBy = "movies")
    private List<Cinema> cinemas = new ArrayList<>();

    public Movie(int id, String title, String poster, Director director) {
        this.id = id;
        this.title = title;
        this.poster = poster;
        this.director = director;
    }

    public void addCinema(Cinema c) {
        this.cinemas.add(c);
    }


}
