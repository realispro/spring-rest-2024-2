package vod.web;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class HelloController {

    @Value("${vod.hello.name:Joe}")
    private String name;

    // /hello?name=Marcin
    @RequestMapping(path="/hello", method= RequestMethod.GET)
    String sayHi(@RequestParam(value = "name", required = false) String nameParam){
        log.info("about to say hi");
        if(nameParam!=null) {
            return "Hey " + nameParam + "!";
        } else {
            return "Hey " + name + "!";
        }
    }
}
