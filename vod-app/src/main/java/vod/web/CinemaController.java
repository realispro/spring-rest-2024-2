package vod.web;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import vod.model.Cinema;
import vod.model.Movie;
import vod.service.CinemaService;
import vod.service.MovieService;

import java.util.List;

@RestController
@Slf4j
@RequiredArgsConstructor
public class CinemaController {

    private final CinemaService cinemaService;
    private final MovieService movieService;

    // get all cinemas
    @GetMapping(value = "/cinemas")
    List<Cinema> getCinemas(
            @RequestHeader(value = "Foo", required = false) String fooHeader,
            @CookieValue(value = "foo", required = false) String fooCookie){
        log.info("about to retrieve cinemas");
        log.info("Foo header: {}", fooHeader);
        log.info("Foo cookie: {}", fooCookie);
        List<Cinema> cinemas = cinemaService.getAllCinemas();
        log.info("{} cinemas found", cinemas.size());
        return cinemas;
    }

    // get cinema
    @GetMapping("/cinemas/{cinemaId}")
    ResponseEntity<Cinema> getCinema(@PathVariable("cinemaId") int cinemaId){
        log.info("about to retrieve cinema {}", cinemaId);

        Cinema cinema = cinemaService.getCinemaById(cinemaId);
        if(cinema==null){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } else {
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(cinema);
        }
    }

    // /movies/{movieId}/cinemas
    @GetMapping("/movies/{movieId}/cinemas")
    ResponseEntity<List<Cinema>> getCinemasShowingMovie(@PathVariable("movieId") int movieId){
        log.info("about to retrieve cinemas showing movie {}", movieId);
        Movie movie = movieService.getMovieById(movieId);
        if(movie!=null) {
            List<Cinema> cinemas = cinemaService.getCinemasByMovie(movie);
            return ResponseEntity.ok(cinemas);
        }else {
            return ResponseEntity.notFound().build();
        }
    }
}
