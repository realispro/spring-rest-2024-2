package vod.web;

import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import vod.model.Director;
import vod.model.Movie;
import vod.service.MovieService;

import java.net.URI;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@RestController
@Slf4j
@RequiredArgsConstructor
@PropertySource("classpath:/codes.properties")
public class MovieController {

    private final MovieService movieService;
    private final MessageSource messageSource;
    private final LocaleResolver localeResolver;

    private final Environment env;

    //get all movies..
    @GetMapping("/movies")
    List<Movie> getMovies() {
        log.info("about to retrieve movies");
        List<Movie> movies = movieService.getAllMovies();
        log.info("{} movies found", movies.size());
        return movies;
    }

    //get movie..
    @GetMapping("/movies/{movieId}")
    ResponseEntity<Movie> getMovie(@PathVariable("movieId") int movieId) {
        log.info("about to retrieve movie {}", movieId);
        Movie movie = movieService.getMovieById(movieId);
        if (movie == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } else {
            return ResponseEntity.status(HttpStatus.OK).body(movie);
        }
    }

    // /directors/{directorId}/movies?title=oko
    @GetMapping("/directors/{directorId}/movies")
    ResponseEntity<List<Movie>> getMoviesByDirector(
            @PathVariable("directorId") int directorId,
            @RequestParam(value = "title", required = false) String title) {
        log.info("about to retrieve movies directed by {}", directorId);
        Director director = movieService.getDirectorById(directorId);
        if (director != null) {
            List<Movie> movies = movieService.getMoviesByDirector(director);

            if (title != null && !title.trim().isEmpty()) {
                movies = movies.stream()
                        .filter(movie -> movie.getTitle().contains(title))
                        .toList();
            }

            return ResponseEntity.ok().body(movies);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping("/movies")
    ResponseEntity<?> addMovie(@Validated @RequestBody Movie movie, Errors errors, HttpServletRequest request) {
        log.info("about to add movie {}", movie);

        if (errors.hasErrors()) {

            Locale locale = localeResolver.resolveLocale(request);

            List<String> messages = errors.getAllErrors().stream()
                    /*.map(objectError -> messageSource.getMessage(
                            objectError.getCode(),
                            objectError.getArguments(),
                            locale
                            ))*/
                    //.map(objectError -> objectError.getCode())
                    .map(objectError -> env.getProperty("error." + objectError.getCode(), objectError.getCode()))
                    .toList();

            return ResponseEntity.badRequest().body(messages);
        }

        if (movie.getTitle().equals("boom")) {
            throw new IllegalStateException("boom");
        }

        movie = movieService.addMovie(movie);

        URI uri = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .build(Map.of(
                        "id", movie.getId()
                ));

        return ResponseEntity
                //.status(HttpStatus.CREATED)
                .created(uri)
                .body(movie);
    }



}
