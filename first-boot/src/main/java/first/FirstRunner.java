package first;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@RequiredArgsConstructor
public class FirstRunner implements CommandLineRunner {


    private final HelloComponent helloComponent;

    @Value("${first.runner.skip:false}")
    private boolean skip;

    @Override
    public void run(String... args) throws Exception {
        if((args.length>0 && args[0].equals("skip")) || skip){
            log.info("skipping runner");
            return;
        }
        log.info("component says hi: {}", helloComponent.sayHi());
    }
}
