package first;

import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class HelloComponent {

    //private static final Logger log = LoggerFactory.getLogger(HelloComponent.class);

    @Value("${first.hello.name:Jane}")
    private String name;

    @PostConstruct
    public void postConstruct(){
        log.info("hello {}! component created", name);
    }

    public String sayHi(){
        return "Hey " + name + "!";
    }

}
